SHELL := /bin/bash

all: build-node build-go

build-node:
	docker build \
		-t node-service:1.0.0 \
		-f dockerfile.subscriber \
		.
	docker system prune -f

build-go:
	docker build \
		-t go-service:1.0.0 \
		-f dockerfile.publisher \
		.
	docker system prune -f

up:
	docker-compose up -d

down:
	docker-compose stop && docker-compose rm -f && docker system prune -f
