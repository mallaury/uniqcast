# uniqcast

## Local installation

This project requires docker to run on local machine.

### Go modules

This project is using Go Module support starting with go1.11. By default modules are set to `auto`. To use `go mod` in `/gitlab.com/mallaury/uniqcast/go` run:

```
export GO111MODULE=on
```

Provision project dependecies:

```
go mod vendor
```

### Getting the project

You can use the traditional `go get` command to download this project into your configured GOPATH.

```
$ go get -u gitlab.com/mallaury/uniqcast
```

## Running The project

All the source code, including any dependencies, have been vendored into the project. There is a single `dockerfile` for each service and a `docker-compose` file that knows how to run all the services.

A `makefile` has also been provide to make building, running and testing the software easier.

### Building the project

Navigate to the root of the project and use the `makefile` to build all of the services.

```
$ cd $GOPATH/gitlab.com/mallaury/uniqcast
$ make all
```

### Running the project

Navigate to the root of the project and use the `makefile` to run all of the services.

```
$ cd $GOPATH/gitlab.com/mallaury/uniqcast
$ make up
```

The `make up` command will leverage Docker Compose to run all the services, including the 3rd party services. The first time to run this command, Docker will download the required images for the 3rd party services.

Default configuration is set which should be valid for most systems. Use the `docker-compose.yaml` file to configure the services differently is necessary.

### Stopping the project

To stop containers:

```
$ make down
```