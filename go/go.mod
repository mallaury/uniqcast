module gitlab.com/mallaury/uniqcast/go

require (
	github.com/google/uuid v1.0.0
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/nats-io/go-nats v1.6.0
	github.com/nats-io/nuid v1.0.0 // indirect
)
