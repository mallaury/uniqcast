package nats

import (
	"encoding/json"
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/nats-io/go-nats"
)

type Content struct {
	Id         int64  `json:"id"`
	ContentUid string `json:"content_uid"`
}

func GetNatsConnection(natsUri string, log *log.Logger) *nats.Conn {

	log.Printf("connecting to nats server: %v", natsUri)

	conn, err := nats.Connect(natsUri, nats.MaxReconnects(5), nats.ReconnectWait(2*time.Second))
	if err != nil {
		log.Fatalf("%v", err)
	}

	return conn
}

func PublishMessageOnTopic(conn *nats.Conn, id int64, ec chan<- error, log *log.Logger) {

	c := Content{
		Id:         id,
		ContentUid: uuid.New().String(),
	}

	cj, err := json.Marshal(c)
	if err != nil {
		log.Printf("error parsing to json: %v", err)
		return
	}

	err = conn.Publish("content.add", []byte(cj))
	if err != nil {
		ec <- err
		return
	}

	log.Printf("published mesage: %v", c)
}
