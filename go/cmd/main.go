package main

import (
	"log"
	"os"
	"sync/atomic"
	"time"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/mallaury/uniqcast/go/cmd/nats"
)

func main() {

	log := log.New(os.Stdout, "NATS PRODUCER : ", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)

	var cfg struct {
		NatsUri string `default:"nats://localhost:4222" envconfig:"NATS_URI"`
	}

	// get nats uri from environment variable
	if err := envconfig.Process("", &cfg); err != nil {
		log.Fatalf("parsing configuration : %v", err)
	}

	// get nats client conenction
	nc := nats.GetNatsConnection(cfg.NatsUri, log)
	defer nc.Close()

	// publish content message every 3 seconds
	ticker := time.NewTicker(3 * time.Second)
	defer ticker.Stop()

	ec := make(chan error, 1)
	defer close(ec)

	var id int64 = 0

	for {
		select {
		case <-ticker.C:
			go func() {
				nats.PublishMessageOnTopic(nc, id, ec, log)
				atomic.AddInt64(&id, 1)
			}()
		case err := <-ec:
			log.Printf("error publishing message: %v", err)
			return
		}
	}
}
