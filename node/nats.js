var NATS = require('nats');

module.exports = {
    
    // initialize nats connection
    initilizeNatsConnection(nats_uri) {

        if(nats_uri === undefined){
            nats_uri = 'nats://localhost:4222'
        }

        var nats = NATS.connect(nats_uri);

        // exit process on nats connection error
        nats.on('error', function(e) {
            console.log('error [' + nats.options.url + ']: ' + e);
            process.exit(1);
        });

        // on nats connection close stop process
        nats.on('close', function() {
            console.log('closed');
            process.exit(1);
        });

        return nats;
    },

    // subscribe to queue
    subscribeToQueue(nats, emitter) {
        return nats.subscribe('content.add', function(msg) {

            content = JSON.parse(msg);
            emitter.emit('content', content);

        });
    }
}