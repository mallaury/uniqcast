var NATS = require('./nats');
var SEQ = require('./db');

var EventEmitter = require('events').EventEmitter;
var emitter = new EventEmitter();

var nats = NATS.initilizeNatsConnection(process.env.NATS_URI);
NATS.subscribeToQueue(nats, emitter);

var SeqConnection = SEQ.initilizePostgresConnection(process.env.POSTGRES_URI);
var ContentModel = SEQ.defineContentModel(SeqConnection);

SEQ.saveBulkContent(emitter, ContentModel);

[`SIGINT`, `SIGUSR1`, `SIGUSR2`, `SIGTERM`].forEach((eventType) => {
    process.on(eventType, function() { 
        console.log('closing nats connection');
        nats.close();

        console.log('closing postgres connection');
        SeqConnection.close();
    });
});