const Sequelize = require('sequelize');

module.exports = {
    
    // initialize postgres connection
    initilizePostgresConnection(postgres_uri) {

        if(postgres_uri === undefined){
            postgres_uri = 'postgres://db_user:db_pass@localhost:5432/content'
        }

        var sequelize = new Sequelize(postgres_uri, {
            dialect: 'postgres',
            operatorsAliases: false,
        
            pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
            },

        });

        sequelize
        .authenticate()
        .then(() => {
            console.log('connection has been established successfully.');
        })
        .catch(err => {
            console.error('unable to connect to the database:', err);
            process.exit(1);
        });

        return sequelize;
    },

    defineContentModel(SeqConnection) {
        return SeqConnection.define('content',
        {
            id: { type: Sequelize.INTEGER, primaryKey: true },
            content_uid: Sequelize.STRING,
            expires: Sequelize.DATE
        },
        {
            timestamps: false,
            freezeTableName: true,
            tableName: 'content'
        });
    },

    saveBulkContent(emitter, Content) {

        var contents = [];

        emitter.on('content', function (content) {
            // increase date by 2 days
            date = new Date();
            date.setDate(date.getDate() + 2);
            
            // make bulk insert
            contents.push({ id: content.id, content_uid: content.content_uid, expires: date });

            if(contents.length == 5) {
                Content.bulkCreate(contents).catch(error => { console.log(error); });
                contents = [];
            }    
        });
    }
}